// router define

import {Router} from "express"

let firstRouter = Router()

firstRouter
.route("/") // local host:8000
.post((req,res)=>{
    res.json("home post")
})
.get((req,res)=>{
    res.json("home get")
})
.patch((re,res)=>{
    res.json("home patch")
})
.delete((req,res)=>{
    res.json("home delete")
})


firstRouter
.route("/name")
.post((req,res)=>{
    res.json("home post")
})
.get((req,res)=>{
    res.json("home get")
})
.patch((req,res)=>{
    res.json("home patch")
})
.delete((req,res)=>{
    res.json("home delete")
})

export default firstRouter;
